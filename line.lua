
Line = {}
Line.x = 0
Line.y = 0
Line.x2 = 0
Line.y2 = 0
Line.color = {}
Line.color.r = 255
Line.color.g = 255
Line.color.b = 255
Line.color.a = 255

function Line:new(x,y,x2,y2,color)
	local newLine = {}
	setmetatable(newLine, self)
	self.__index = self

	newLine.x = x
	newLine.y = y
	newLine.x2 = x2
	newLine.y2 = y2
	newLine.color = newColor(color.r, color.g, color.b, color.a)

	return newLine
end

function Line:draw()
	love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.a)
	love.graphics.line(self.x, self.y, self.x2, self.y2)
end