 
Rectangle = {}

function Rectangle:new(x,y,w,h,color,fill)
	local newRectangle = {}
	setmetatable(newRectangle, self)
	self.__index = self

	newRectangle.x = x
	newRectangle.y = y
	newRectangle.w = w
	newRectangle.h = h
	newRectangle.fill = fill

	newRectangle.color = newColor(color.r, color.g, color.b, color.a)


	return newRectangle
end

function Rectangle:getCenter()
	local center = {}
		center.x = (self.x+self.w)/2
		center.y = (self.y+self.w)/2
		return center
end

function Rectangle:SetColor(r,g,b,a)

	self.color.r=r
	self.color.g=g
	self.color.b=b
	self.color.a=a


end

function Rectangle:draw()
	love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.a)
	love.graphics.rectangle(self.fill, self.x, self.y, self.w, self.h)
end

function Rectangle:getX()
	return self.x
end
function Rectangle:getY()
	return self.y
end
function Rectangle:getW()
	return self.w
end
function Rectangle:getH()
	return self.h
end
