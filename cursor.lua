
Cursor = {}

--i need to make it so you can create different cursors by passing in lines, and rects, later graphics,
--have a file with cursor styles? if you want a arrow it looks up what that is then that returns the table
--the table with the shapes, and 
function Cursor:new()
	local newCursor = {}
	setmetatable(newCursor, self)
	self.__index = self

	newCursor.x = 0
	newCursor.y = 0
	newCursor.x2 = 0
	newCursor.y2 = 0
	newCursor.color = newColor(255,50,50,255)
	newCursor.vertLine = Line:new(x,y,x2,y2,newCursor.color)
	newCursor.topLine = Line:new(x,y,x2,y2,newCursor.color)
	newCursor.botLine = Line:new(x,y,x2,y2,newCursor.color)
	newCursor.stemLine = Line:new(x,y,x2,y2,newCursor.color)

	return newCursor
end

function Cursor:draw()
	if love.window.hasMouseFocus() then
	love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.a)
	self.vertLine:draw()
	self.topLine:draw()
	self.botLine:draw()
	self.stemLine:draw()
	end

end

function Cursor:update()


	self.vertLine.x=love.mouse.getX()
	self.vertLine.y=love.mouse.getY()
	self.vertLine.x2=love.mouse.getX()+5
	self.vertLine.y2=love.mouse.getY()+15
	self.topLine.x = love.mouse.getX()
	self.topLine.y = love.mouse.getY()
	self.topLine.x2 = love.mouse.getX()+13
	self.topLine.y2 = love.mouse.getY()+8

	self.botLine.x = love.mouse.getX()+5
	self.botLine.y = love.mouse.getY()+15
	self.botLine.x2 = love.mouse.getX()+13
	self.botLine.y2 = love.mouse.getY()+8

	self.stemLine.x = love.mouse.getX()+9
	self.stemLine.y = love.mouse.getY()+11
	self.stemLine.x2 = love.mouse.getX()+13
	self.stemLine.y2 = love.mouse.getY()+16
end

function Cursor:arrow()


end

function Cursor:box()

end
