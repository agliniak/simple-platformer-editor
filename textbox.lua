Textbox = {}

function Textbox:new(x,y,size) --add font pass in later
	local newTextbox = {}
	setmetatable(newTextbox, self)
	self.__index = self

	newTextbox.color = newColor(255,255,255,255)
	newTextbox.string = 0
	--newtextbox = love.graphics.newFont(size)
	love.graphics.setNewFont(size)
	newTextbox.x = x
	newTextbox.y = y
	return newTextbox
end



function Textbox:draw()
	love.graphics.setColor(self.color.r,self.color.g,self.color.b,self.color.a)
	love.graphics.print(self.string,self.x, self.y)
end

function Textbox:update()
--is there a new string?

end

function Textbox:SetColor(r,g,b,a)

	self.r=r
	self.g=g
	self.b=b
	self.a=a


end

function Textbox:updateString(newString)
	self.string = newString

end