Button = {}
--HOW DO I GET THE BUTTONS TO KNOW ABOUT THE STATEMACHINE!!!

--IDEA!  add a group number,  so the statmachin can remove all states of a broup
function Button:new(x,y,w,h,color,label,variable,stateMachine)
	local newButton= {}
	setmetatable(newButton, self)
	self.__index = self


	--button should have internalstates:  hover, pressed, unpressed

	newButton.pstateMachine = stateMachine

	--outlinerect
	newButton.hoverColor = newColor(color.r+50,color.g+50,color.b+50,255)
	newButton.buttonColor = newColor(color.r,color.g,color.b,color.a)
	
	newButton.outlineRect = Rectangle:new(x-1,y-1,w+2,h+2,self:MakeOutlineColor(color),"line")
	--fillrect
	newButton.button = Rectangle:new(x,y,w,h,color,"fill")
	newButton.buttonVariable = variable or "NO VAR"
	--maybe make it so you pass in the rect and it will auto center in text box?
	-- for now we do it manualy

	newButton.label = label or "NONE"
	newButton.textbox = Textbox:new(x,y,15)
	newButton.textbox:updateString(newButton.label)
	newButton.textbox:SetColor(255,255,255,255)
	--make a table holding all of these?
	--then draw all aspects,
	--outline thickness?

	return newButton
end


-- button checks the state, so it can stay pressed
function Button:draw()
	self.outlineRect:draw()
	self.button:draw()
	self.textbox:draw()
	
end

function Button:update()
	--self.outlineRect:update()   these rects dont need updateing,  going out of my mind
	--self.button:update()
	self.textbox:update()
	--hover?
	--clicked on state
	--self:MouseHover()
	self:MouseClick()
	
end

function Button:MouseHover()

	-- check mouse postion is inside button rect
	if love.mouse.getX() <= self.button.x+self.button.w and
   		love.mouse.getX() >= self.button.x and
  		love.mouse.getY() <= self.button.y+self.button.h and
   		love.mouse.getY() >= self.button.y then

   		--print("MOUSE IN BUTTON!!!")
   		--change color
   		self.button:SetColor(self.hoverColor.r,self.hoverColor.g,self.hoverColor.b,self.hoverColor.a)
  	return true
	else
		--change color back
		--MAKE HOVER COLOR!!
		self.button:SetColor(self.buttonColor.r,self.buttonColor.g,self.buttonColor.b,self.buttonColor.a)

	return false
	end
end


function Button:MouseClick()
--move all mouse clicks and keyboard events to seperet input handler
	-- if not in edit window

	if self:MouseHover() then
		if love.mouse.isDown("l") then
			--print(self.buttonVariable)
			self.pstateMachine:AddState(self.buttonVariable)

			--correct this with better states
			if self.buttonVariable == "PLATFORM_MODE" then

				self.pstateMachine:RemoveState("SPAWN_POINT_MODE")
				self.pstateMachine:RemoveState("END_POINT_MODE")

				elseif self.buttonVariable == "SPAWN_POINT_MODE" then

					self.pstateMachine:RemoveState("PLATFORM_MODE")
					self.pstateMachine:RemoveState("END_POINT_MODE")

				elseif self.buttonVariable == "END_POINT_MODE" then
					self.pstateMachine:RemoveState("SPAWN_POINT_MODE")
					self.pstateMachine:RemoveState("PLATFORM_MODE")

			end

			--if passed in state machine pointer then add state
		end
		
	end


end




function Button:MakeOutlineColor(color)

	--need to make a algorythm to make good outline colors
	tempColor = {}
	tempColor.r = 200
	tempColor.g = 200
	tempColor.b = 200
	tempColor.a = 255

	return tempColor

end